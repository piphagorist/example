﻿using System;



namespace SG.CoreLogic.Configs
{
    public class ConfigPage : Attribute
    {
        public string Name;



        public ConfigPage(string name)
        {
            Name = name;
        }
    }



    public class DefaultIfEmpty : Attribute
    {
        public object Value;



        public DefaultIfEmpty(object value)
        { 
            Value = value;
        }
    }



    public class CanBeNull : Attribute { }
}