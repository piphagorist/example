﻿namespace SG.CoreLogic.Configs
{
    public interface IConfig {}

    public interface IWithIdConfig : IConfig
    {
        string ID { get; }
    }

    public class ConfigWithId : IWithIdConfig
    {
        public string ID { get; private set; }



        public ConfigWithId(string id)
        {
            ID = id;
        }
    }

    public class ConfigWithIdSprite : ConfigWithId
    {
        public readonly string SpriteID;



        public ConfigWithIdSprite(string id, string spriteId) : base(id)
        {
            SpriteID = spriteId;
        }
    }
}