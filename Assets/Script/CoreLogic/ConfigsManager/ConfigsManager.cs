using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace SG.CoreLogic.Configs
{
    public sealed class ConfigsManager
    {
        private Dictionary<Type, List<IConfig>> _configs = new Dictionary<Type, List<IConfig>>();

        private readonly Dictionary<string,IWithIdConfig > _byIdCache = new Dictionary<string, IWithIdConfig>();


        public ConfigsManager(string jsonData)
        {
            Dictionary<string, List<object>> data = 
                JsonConvert.DeserializeObject<Dictionary<string, List<object>>>(jsonData);

            foreach (var configData in data)
            {
                Type type = Type.GetType(configData.Key);
                _configs.Add(type, new List<IConfig>());

                foreach (var dataItem in configData.Value)
                {
                    var cfg = JsonConvert.DeserializeObject(dataItem.ToString(), type) as IConfig;
                    var isWithIdConfig = ((IList) type.GetInterfaces()).Contains(typeof(IWithIdConfig));
                    if (isWithIdConfig)
                    {
                        if (cfg is IWithIdConfig asWithId)
                        {
                            
                            try
                            {
                                if (!_byIdCache.ContainsKey(asWithId.ID))
                                    _byIdCache.Add(asWithId.ID, asWithId);
                            }
                            catch (Exception e)
                            {
                                Debug.LogWarning($"can not add {asWithId.ID} to by id cache : {e.Message}");
                            }
                        }
                    }

                    _configs[type].Add(cfg);
                }
            }
        }

        public bool TryGetConfig<T>(string id, out T result) where T : ConfigWithId
        {
            var hasValue = _byIdCache.TryGetValue(id, out var r);
            result = r as T;
            return hasValue;
        }

        public T GetConfig<T>(string id) where T : ConfigWithId
        {
            _byIdCache.TryGetValue(id, out var r);
            return r as T;
        }
        public T GetConfig<T>() where T : IConfig => (T)_configs[typeof(T)][0];
        public IReadOnlyList<T> GetConfigsList<T>() where T : IConfig => _configs[typeof(T)].ConvertAll(e => (T)e);
    }
}
