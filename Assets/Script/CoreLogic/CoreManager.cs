using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using SG.CoreLogic.Events;
using SG.CoreLogic.Configs;
using SG.CoreLogic.Server;

namespace SG.CoreLogic
{
    public sealed class CoreManager
    {
        internal EventsManager EventsManager;
        internal ConfigsManager ConfigsManager;
        internal Random Random;
        internal TimeManager TimeManager;
        internal IServerManager ServerManager;
        internal ServerGameData ServerGameData;


        private Dictionary<Type, IManager> _managers;



        public CoreManager(Assembly assembly, EventsManager eventManager, ConfigsManager configsManager,
            Random random, TimeManager timeManager, IServerManager serverManager, ServerGameData serverGameData)
        {
            EventsManager = eventManager;
            ConfigsManager = configsManager;
            Random = random;
            TimeManager = timeManager;
            ServerManager = serverManager;
            ServerGameData = serverGameData;

            CreateManagers(GetManagers(assembly));
        }

        public T GetManager<T>() where T : IManager => (T)_managers[typeof(T)];


        private void CreateManagers(IEnumerable<Type> types)
        {
            _managers = new Dictionary<Type, IManager>();

            foreach (Type type in types)
            {
                IManager instance = (IManager)Activator.CreateInstance(type);
                _managers.Add(type, instance);
                
                foreach (Type i in type.GetInterfaces())
                {
                    if (i.GetInterfaces().Contains(typeof(IManager)))
                    {
                        _managers.Add(i, instance);
                        break;
                    }
                }
            }

            foreach (KeyValuePair<Type, IManager> pair in _managers)
            {
                IManager manager = pair.Value;
                if (manager.GetType().IsInterface) continue;

                manager.Setup();
                manager.Inject(this);
                manager.Init();
            }

            foreach (KeyValuePair<Type, IManager> pair in _managers)
            {
                pair.Value.PostInit();
            }
        }

        private IEnumerable<Type> GetManagers(Assembly assembly)
        {
            return assembly.GetTypes().Where(e => e.GetInterfaces().Contains(typeof(IManager)) && !e.IsAbstract).
                OrderBy(e => 
                {
                    InitOrderAttribute orderAttribute = e.GetCustomAttribute<InitOrderAttribute>();

                    return orderAttribute?.Order ?? int.MaxValue;
                } 
                );
        }
    }
}
