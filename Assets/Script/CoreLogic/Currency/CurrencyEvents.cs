﻿using SG.CoreLogic.Events;

namespace SG.CoreLogic.Currency
{
    public readonly struct CurrencyValueChangedEvent : IEvent
    {
        public readonly CurrencyType CurrencyType;
        public readonly double Currency;



        public CurrencyValueChangedEvent(CurrencyType currencyType, double currency)
        {
            CurrencyType = currencyType;
            Currency = currency;
        }
    }
}