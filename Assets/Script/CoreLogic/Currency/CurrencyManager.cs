﻿using System;
using System.Collections.Generic;
using SG.CoreLogic.Server;

namespace SG.CoreLogic.Currency
{
    [InitOrder(2)]
    public sealed class CurrencyManager : Manager
    {
        public double SoftCurrency => _currency[CurrencyType.Soft];
        public double HardCurrency => _currency[CurrencyType.Hard];


        private Dictionary<CurrencyType, double> _currency;
        private double _accumulatedSoftCurrency;



        protected override void InternalInit()
        {
            _currency = new Dictionary<CurrencyType, double>();

            _currency.Add(CurrencyType.Soft, GameData.player.coins);
            _currency.Add(CurrencyType.Hard, GameData.player.diamonds);

            SubscribeTo<ResponseGotEvent>(UpdateCurrencyHandler);
        }


        private void UpdateCurrencyHandler(ResponseGotEvent _)
        {
            _currency[CurrencyType.Soft] = GameData.player.coins;
            _currency[CurrencyType.Hard] = GameData.player.diamonds;

            Push(new CurrencyValueChangedEvent(CurrencyType.Soft, _currency[CurrencyType.Soft]));
            Push(new CurrencyValueChangedEvent(CurrencyType.Hard, _currency[CurrencyType.Hard]));
        }
    }
}