using System;
using System.Reflection;
using System.Collections.Generic;



namespace SG.CoreLogic.Events
{
    public sealed class EventsManager
    {
        private Dictionary<Type, List<IEventContainer>> _cachedEvents;



        public EventsManager()
        {
            _cachedEvents = new Dictionary<Type, List<IEventContainer>>();
        }

        public void Push<T>(T data) where T : IEvent
        {
            Type type = typeof(T);

            CreateListIfNull(type);

            List<IEventContainer> cache = _cachedEvents[type];

            foreach (IEventContainer container in cache)
            {
                ((EventContainer<T>)container).Invoke(data);
            }
        }

        public void SubscribeTo<T>(Action<T> action) where T : IEvent
        {
            Type type = typeof(T);
            
            CreateListIfNull(type);

            if (IsSubscribed(type, action)) return;

            _cachedEvents[type].Add(new EventContainer<T>(action));
        }

        public void UnSubscribeFrom<T>(Action<T> action) where T : IEvent
        {
            Type type = typeof(T);

            CreateListIfNull(type);

            if (!IsSubscribed(type, action)) return;

            List<IEventContainer> cache = _cachedEvents[type];

            IEventContainer container = cache.Find(e => e.MethodInfo == action.Method);

            cache.Remove(container);
        }


        private void CreateListIfNull(Type type)
        {
            if (!_cachedEvents.ContainsKey(type))
            {
                _cachedEvents.Add(type, new List<IEventContainer>());
                return;
            }

            if (_cachedEvents[type] == null)
            {
                _cachedEvents[type] = new List<IEventContainer>();
                return;
            }
        }

        private bool IsSubscribed<T>(Type type, Action<T> action) where T : IEvent
        {
            if (!_cachedEvents.ContainsKey(type) || _cachedEvents[type] == null)
                return false;

            List<IEventContainer> cache = _cachedEvents[type];

            foreach (IEventContainer container in cache)
            {
                if (IsEquals(action, container))
                    return true;
            }

            return false;
        }

        private bool IsEquals<T>(Action<T> action, IEventContainer container) where T : IEvent
        {
            return action.Method == container.MethodInfo;
        }
        


        private class EventContainer<T>: IEventContainer where T : IEvent
        {
            private Action<T> _action;


            public MethodInfo MethodInfo { get; }



            public EventContainer(Action<T> action)
            {
                _action = action;
                MethodInfo = action.Method;
            }

            public void Invoke(T data) => _action.Invoke(data);
        }



        private interface IEventContainer
        {
            MethodInfo MethodInfo { get; }
        }
    }



    public interface IEvent {}
}
