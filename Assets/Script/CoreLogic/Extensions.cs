﻿using System.Linq;
using System.Collections.Generic;
using SG.CoreLogic.Configs;
using System;
using UnityEngine;

namespace SG.CoreLogic
{
    public static class Extensions
    {

        public static bool TryGetById<T>(this IReadOnlyList<T> list, string id, out T result) where T : ConfigWithId
        {
            for (var index = 0; index < list.Count; index++)
            {
                var obj = list[index];
                if (obj.ID != id) continue;
                result = obj;
                return true;
            }

            result = null;
            return true;
        }
        public static T GetByID<T>(this IReadOnlyList<T> list, string id) where T : ConfigWithId
        {
            return list.FirstOrDefault(e => e.ID == id, null);
        }

        public static T LastElement<T> (this IReadOnlyList<T> list)
        {
            return list[list.Count - 1];
        }

        public static double Divide(this TimeSpan devidend, TimeSpan devider)
        {
            return devidend.TotalMilliseconds / devider.TotalMilliseconds;
        }

        public static TimeSpan Multiply(this TimeSpan multiplier1, double multiplier2)
        {
            return TimeSpan.FromMilliseconds(multiplier1.TotalMilliseconds * multiplier2);
        }

        public static TimeSpan AddSeconds(this TimeSpan ts, double seconds)
        {
            return TimeSpan.FromMilliseconds(ts.TotalMilliseconds + seconds * 1000);
        }

        public static T FirstOrDefault<T>(this IEnumerable<T> list, Func<T, bool> predicate, T defaultObj)
        {
            foreach (T item in list)
                if (predicate(item))
                    return item;

            return defaultObj;
        }

        public static T GetRandomElement<T>(this IReadOnlyList<T> list, System.Random r)
        {
            if (list == null) return default;

            return list[r.Next(0, list.Count)];
        }
    }
}