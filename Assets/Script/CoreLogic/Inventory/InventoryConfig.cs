using SG.CoreLogic.Configs;
using Newtonsoft.Json;

namespace SG.CoreLogic.Inventory
{
    [ConfigPage("ItemConfig")]
    public class ItemConfig : ConfigWithId
    {
        public readonly ItemType Type;
        public readonly string SpriteId;



        [JsonConstructor]
        public ItemConfig(string id, ItemType type, [CanBeNull] string spriteId) : base(id)
        {
            Type = type;
            SpriteId = spriteId;
        }
    }
}