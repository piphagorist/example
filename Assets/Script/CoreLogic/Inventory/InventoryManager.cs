using System;
using System.Collections.Generic;
using SG.CoreLogic.Server;

namespace SG.CoreLogic.Inventory
{
    public class InventoryManager : Manager
    {
        private IReadOnlyList<ItemConfig> _itemsConfig;



        protected override void InternalInject()
        {
            _itemsConfig = GetConfigsList<ItemConfig>();
        }


        public int GetItemsCount(string id)
        {
            InventoryItem item = GameData.inventory.FirstOrDefault(e => e.itemId == id, null);

            if (item == null) return 0;
            return item.count;
        }
    }
}