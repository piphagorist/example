namespace SG.CoreLogic.Localization
{
    public interface ILocalizationManager
    {
        void SetLanguage(string languageCode);
        string GetValueByKey(string key, params object[] args);
    }
}