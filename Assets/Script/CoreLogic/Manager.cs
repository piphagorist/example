﻿using System;
using System.Linq;
using System.Collections.Generic;
using SG.CoreLogic.Configs;
using SG.CoreLogic.Events;
using SG.CoreLogic.Server;

namespace SG.CoreLogic
{
    public abstract class Manager : IManager
    {
        protected CoreManager CoreManager;
        protected Random Random;
        protected ServerGameData GameData;



        void IManager.Setup() => InternalSetup();
        void IManager.Init() => InternalInit();
        void IManager.Inject(CoreManager coreManager) => InternalInject(coreManager);
        void IManager.PostInit() => OnManagerLoaded();


        protected virtual void InternalInject(CoreManager coreManager)
        {
            CoreManager = coreManager;

            Random = CoreManager.Random;

            GameData = coreManager.ServerGameData;

            InternalInject();
        }
        protected virtual void InternalSetup() { }
        protected virtual void InternalInject() { }
        protected virtual void InternalInit() { }
        protected virtual void OnManagerLoaded() { }
        protected void Push<T>(T data) where T : IEvent => CoreManager.EventsManager.Push(data);
        protected void SubscribeTo<T>(Action<T> action) where T : IEvent => CoreManager.EventsManager.SubscribeTo(action);
        protected T GetConfig<T>() where T : IConfig => CoreManager.ConfigsManager.GetConfig<T>();
        protected IReadOnlyList<T> GetConfigsList<T>() where T : IConfig => CoreManager.ConfigsManager.GetConfigsList<T>();

        protected T GetConfigById<T>(IReadOnlyList<T> configs, string id) where T : ConfigWithId
        {
            return configs.First(e => e.ID == id);
        }
    }



    public interface IManager
    {
        internal void Setup();
        internal void Inject(CoreManager coreManager);
        internal void Init();
        internal void PostInit();
    }



    public class InitOrderAttribute : Attribute
    {
        public int Order;

        public InitOrderAttribute(int order)
        {
            Order = order;
        }
    }
}