using SG.CoreLogic.Events;

namespace SG.CoreLogic.Server
{
    public readonly struct RequestSentEvent : IEvent { }
    public readonly struct ResponseGotEvent : IEvent { }
}