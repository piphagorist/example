﻿using System;

namespace SG.CoreLogic.Server
{
    public interface IServerManager
    {
        long RequestTime { get; }
        void SendToServer(BaseRequestJsonData data, Action callback, Action onError, bool withoutSentEvent = false);
        void SetPlayerID(string playerID);
        void SetSessionKey(string sessionKey);
        void Inject();
    }
}