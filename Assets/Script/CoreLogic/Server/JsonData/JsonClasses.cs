﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SG.CoreLogic.Server
{
    [Serializable]
    public class Session
    {
        public string sessionKey;
        public bool newPlayer;
    }


    [Serializable]
    public class InventoryItem : ItemIdCount { }

    [Serializable]
    public class Player
    {
        public int level;
        public long experience;
        public double coins;
        public double diamonds;
        public long create;
        public int lastDay;
    }

    [Serializable]
    public class ItemIdCount
    {
        public string itemId;
        public int count;
    }
}