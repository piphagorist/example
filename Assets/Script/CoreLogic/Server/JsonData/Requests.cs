using System;

namespace SG.CoreLogic.Server
{
    [Serializable]
    public abstract class BaseRequestJsonData
    {
        public abstract string method {get;}
    }

    [Serializable]
    public class AuthRequestJsonData : BaseRequestJsonData
    {
        public override string method => "auth";
    }

    [Serializable]
    public class SyncRequestJsonData : BaseRequestJsonData
    {
        public override string method => "sync";
    }
}
