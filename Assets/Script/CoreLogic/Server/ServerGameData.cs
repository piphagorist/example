﻿using System;
using System.Collections.Generic;

namespace SG.CoreLogic.Server
{
    [Serializable]
    public class ServerGameData
    {
        public long serverTime;
        public Session session;
        public Player player;
        public InventoryItem[] inventory;

    }
}
