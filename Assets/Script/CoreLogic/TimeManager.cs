﻿using SG.CoreLogic.Server;
using System;

namespace SG.CoreLogic
{
    public class TimeManager
    {
        public Action<TimeSpan> OnTick;
        public Action OnBackToGame;


        public TimeSpan CurrentTime => _currentTime;


        private IServerManager _serverManager;
        private ServerGameData _gameGata;
        private TimeSpan _currentTime;
        private DateTime _prevSystemTime;
        private float _syncInterval = 60.0f;
        private float _syncIntervalTime;



        public TimeManager(IServerManager serverManager, ServerGameData gameData, long time)
        {
            _serverManager = serverManager;
            _gameGata = gameData;

            SetTime(time);

            _prevSystemTime = DateTime.Now;
        }

        public void Tick(float deltaTime)
        {
            float systemDeltaTime = (float)(DateTime.Now - _prevSystemTime).TotalSeconds;

            if (systemDeltaTime < deltaTime || systemDeltaTime - deltaTime < 0.5f)
                deltaTime = systemDeltaTime;

            TimeSpan deltaTimeSpan = TimeSpan.FromSeconds(deltaTime);
            _currentTime += deltaTimeSpan;

            _syncIntervalTime += deltaTime;
            if (_syncIntervalTime >= _syncInterval)
            {
                _syncIntervalTime = 0;
                SyncTime();
            }

            OnTick?.Invoke(deltaTimeSpan);

            _prevSystemTime = DateTime.Now;
        }

        public void SyncTime(Action onComplete = null, bool afterForusLost = false)
        {
            _serverManager.SendToServer(new SyncRequestJsonData(), () =>
            {
                SetTime(_gameGata.serverTime + _serverManager.RequestTime);
                _syncIntervalTime = 0;
                
                OnBackToGame?.Invoke();
                onComplete?.Invoke();
            }, null, afterForusLost);
        }


        private void SetTime(long time)
        {
            _currentTime = TimeSpan.FromMilliseconds(time);

            _prevSystemTime = DateTime.Now;
        }
    }
}