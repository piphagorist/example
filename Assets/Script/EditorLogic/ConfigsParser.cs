#if UNITY_EDITOR
using System;
using System.IO;
using System.Data;
using System.Reflection;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using SG.CoreLogic.Configs;

namespace SG.EditorLogic
{
    class ConfigParser : ParserBase
    {
        protected override string SheetID => "";



        [MenuItem("Tools/ConfigParser")]
        private static void ShowWindow()
        {
            GetWindow(typeof(ConfigParser)).Show();
        }


        protected override void ParseTables(DataTableCollection collection)
        {
            Dictionary<string, Type> types = new Dictionary<string, Type>();
            Dictionary<string, DataTable> tables = new Dictionary<string, DataTable>();
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings
            {
                Culture = new CultureInfo("ru-RU")
            };

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                foreach (Type type in assembly.GetTypes())
                    if (type.GetCustomAttributes(typeof(ConfigPage), true).Length > 0)
                        types.Add(type.GetCustomAttribute<ConfigPage>().Name, type);

            foreach (DataTable table in collection)
                tables.Add(table.TableName, table);

            Dictionary<string, List<Dictionary<string, object>>> data = new Dictionary<string, List<Dictionary<string, object>>>();

            foreach (var type in types)
            {
                if (tables.ContainsKey(type.Key))
                {
                    var table = tables[type.Key];
                    var typeFullName = type.Value.FullName;

                    data.Add(typeFullName, new List<Dictionary<string, object>>());

                    var parameters = type.Value.GetConstructors()[0].GetParameters();

                    if (!CheckParametersInTable(parameters, table)) return;

                    int emptyRowsCount = 0;

                    for (int rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
                    {
                        if (string.IsNullOrEmpty(table.Rows[rowIndex][0].ToString()))
                        {
                            emptyRowsCount++;
                            continue;
                        }

                        data[typeFullName].Add(new Dictionary<string, object>());

                        foreach (var parameter in parameters)
                        {
                            object value = null;

                            bool defaultIfEmpty = Attribute.IsDefined(parameter, typeof(DefaultIfEmpty));
                            bool canBeNull = Attribute.IsDefined(parameter, typeof(CanBeNull));

                            for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                            {
                                string fieldName = table.Columns[columnIndex].ColumnName;
                                string cellValue = table.Rows[rowIndex][columnIndex].ToString();

                                if (fieldName.ToLower() == parameter.Name.ToLower())
                                {
                                    value = !string.IsNullOrEmpty(table.Rows[rowIndex][columnIndex].ToString()) ?
                                        table.Rows[rowIndex][columnIndex] : null;
                                    break;
                                }
                            }

                            if (value == null)
                            {
                                if (defaultIfEmpty)
                                    value = parameter.GetCustomAttribute<DefaultIfEmpty>().Value;
                                else if (!canBeNull)
                                    throw new Exception($"Parameter {parameter.Name} not found in table {table.TableName} an row {rowIndex + 1}");
                            }

                            try
                            {
                                value = JsonConvert.DeserializeObject($"\"{value}\"", parameter.ParameterType, serializerSettings);
                            }
                            catch
                            {
                                try
                                {
                                    value = JsonConvert.DeserializeObject($"{value}", parameter.ParameterType, serializerSettings);
                                }
                                catch
                                {
                                    throw new Exception($"Parameter {parameter.Name} in table {table.TableName} has wrong value. Type is {parameter.ParameterType}, value {value}");
                                }
                            }

                            data[typeFullName][rowIndex - emptyRowsCount].Add(parameter.Name, value);
                        }
                    }
                }
                else
                {
                    throw new Exception($"Config {type.Key} not exist in Google sheet");
                }
            }

            File.WriteAllText(Application.dataPath + "/Resources/Config.json", JsonConvert.SerializeObject(data, Formatting.Indented, serializerSettings));
            AssetDatabase.ImportAsset("Assets/Resources/Config.json");
            Debug.Log("Complete config parsing!");


            bool CheckParametersInTable(ParameterInfo[] parameters, DataTable table)
            {
                foreach (var paramInfo in parameters)
                {
                    bool paramFound = false;
                    for (int i = 0; i < table.Columns.Count; i++)
                    {
                        if (table.Columns[i].ColumnName.ToLower() == paramInfo.Name.ToLower())
                        {
                            paramFound = true;
                            break;
                        }
                    }

                    if (!paramFound)
                        throw new Exception($"Parameter {paramInfo.Name} not found in table {table.TableName}");
                }

                return true;
            }
        }
    }
}
#endif