using UnityEngine;

namespace SG.EditorLogic
{
    public static class Extensions
    {
        public static Texture2D ChangeFormat(this Texture2D oldTexture, TextureFormat newFormat)
        {
            Texture2D newTex = new Texture2D(oldTexture.width, oldTexture.height, newFormat, false);
            newTex.SetPixels(oldTexture.GetPixels());
            newTex.Apply();

            return newTex;
        }
    }
}