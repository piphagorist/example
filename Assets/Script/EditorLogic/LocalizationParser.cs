#if UNITY_EDITOR
using System.IO;
using System.Data;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using SG.UnityLogic.Localization;

namespace SG.EditorLogic
{
    class LocalizationParser : ParserBase
    {
        protected override string SheetID => "";



        [MenuItem("Tools/LocalizationParser")]
        private static void ShowWindow()
        {
            GetWindow(typeof(LocalizationParser)).Show();
        }


        protected override void ParseTables(DataTableCollection collection)
        {
            string jsonListName = "items";
            List<LocalizationItem> localizationItems;

            foreach (DataTable table in collection)
            {
                for (int columnIndex = 1; columnIndex < table.Columns.Count; columnIndex++)
                {
                    localizationItems = new List<LocalizationItem>();
                    
                    for (int rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
                    {
                        string key = table.Rows[rowIndex][0].ToString();
                        if (string.IsNullOrEmpty(key))
                            continue;
                            
                        string value = table.Rows[rowIndex][columnIndex].ToString();
                        localizationItems.Add(new LocalizationItem() {key = key, value = value});
                    }

                    Dictionary<string, List<LocalizationItem>> jsonData = new Dictionary<string, List<LocalizationItem>>
                    {
                        {
                            jsonListName, localizationItems
                        }
                    };

                    string fileName = table.Columns[columnIndex].ColumnName;
                    string json = JsonConvert.SerializeObject(jsonData);

                    File.WriteAllText($"{Application.dataPath}/Resources/Localization/{fileName}.json", json);
                    AssetDatabase.ImportAsset($"Assets/Resources/Localization/{fileName}.json");
                }
            }

            Debug.Log("Complete localization parsing!");
        }
    }
}
#endif