#if UNITY_EDITOR
using System.IO;
using System.Data;
using UnityEngine;
using UnityEditor;
using UnityGoogleDrive;
using ExcelDataReader;

namespace SG.EditorLogic
{
    public abstract class ParserBase : EditorWindow 
    {
        protected virtual string SheetID { get; private set; }



        private void OnGUI()
        {
            GUILayout.Space(10);

            SheetID = EditorGUILayout.TextField("SheetID", SheetID);

            GUILayout.Space(10);

            if (GUILayout.Button("Parse"))
            {
                ExportSheet();
            }
        }

        private void ExportSheet()
        {
            var exporter = GoogleDriveFiles.Export(SheetID, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            exporter.Send().OnDone += file =>
            {
                if (!exporter.IsError && exporter.IsDone)
                    ParseXLSX(file.Content);
            };
        }

        private void ParseXLSX(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                using (var reader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    var conf = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    };

                    var dataSet = reader.AsDataSet(conf);

                    ParseTables(dataSet.Tables);
                }
            }
        }

        protected abstract void ParseTables(DataTableCollection collection);
    }
}
#endif