#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace SG.EditorLogic
{
    public class SpriteAtlasCreator
    {
        [MenuItem("Assets/CreateAtlas")]
        private static void CreateSpriteAtlas()
        {
            string folderPath = GetFolderPath(Selection.activeObject);

            List<Texture2D> textures = new List<Texture2D>();

            textures.AddRange(GetTexturesFromDirectory(folderPath));

            foreach (string directory in Directory.GetDirectories(folderPath))
            {
                textures.AddRange(GetTexturesFromDirectory(directory));
            }

            Texture2D atlas = new Texture2D(1024, 1024);
            Rect[] rects = atlas.PackTextures(textures.ToArray(), 0, 4096);
            atlas = atlas.ChangeFormat(TextureFormat.RGBA32);

            string path = $"Assets/Resources/Atlases/{Selection.activeObject.name}.png";

            bool fileExists = File.Exists(path);
            if (fileExists)
                AssetDatabase.DeleteAsset(path);
            File.WriteAllBytes(path, atlas.EncodeToPNG());
            AssetDatabase.ImportAsset(path);

            SpriteMetaData[] metas = new SpriteMetaData[rects.Length];
            for (int i = 0; i < metas.Length; i++)
            {
                string texturePath = AssetDatabase.GetAssetPath(textures[i]);
                metas[i].rect = new Rect(atlas.width * rects[i].x, atlas.height * rects[i].y, atlas.width * rects[i].width, atlas.height * rects[i].height);
                metas[i].name = textures[i].name;
                metas[i].border = ((TextureImporter)TextureImporter.GetAtPath(texturePath)).spriteBorder;
            }

            TextureImporterPlatformSettings importSettings = new TextureImporterPlatformSettings();
            importSettings.name = "WebGL";
            importSettings.crunchedCompression = true;
            importSettings.compressionQuality = 100;
            importSettings.format = TextureImporterFormat.DXT5Crunched;
            importSettings.overridden = true;
            importSettings.maxTextureSize = 4096;

            TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(path);
            importer.wrapMode = TextureWrapMode.Clamp;
            importer.textureType = TextureImporterType.Sprite;
            importer.mipmapEnabled = false;
            importer.spriteImportMode = SpriteImportMode.Multiple;
            importer.spritesheet = metas;
            importer.SetPlatformTextureSettings(importSettings);

            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);


            List<Texture2D> GetTexturesFromDirectory(string path)
            {
                List<Texture2D> list = new List<Texture2D>();

                foreach (string objectPath in Directory.GetFiles(path))
                {
                    Texture2D obj = AssetDatabase.LoadAssetAtPath(objectPath, typeof(Texture2D)) as Texture2D;
                    if (obj == null)
                        continue;

                    if (!obj.isReadable)
                    {
                        throw new System.Exception($"Texture {obj.name} is not readable!");
                    }

                    list.Add(obj as Texture2D);
                }

                return list;
            }
        }

        [MenuItem("Assets/CreateAtlas", true)]
        private static bool CheckSprites()
        {
            if (Selection.activeObject != null)
                return IsAssetFolder(Selection.activeObject);
            
            return false;
        }

        private static bool IsAssetFolder(Object obj)
        {
            if (GetFolderPath(obj) == null)
                return false;

            return true;
        }

        private static string GetFolderPath(Object obj)
        {
            string path = AssetDatabase.GetAssetPath(obj.GetInstanceID());

            if (path.Length > 0)
            {
                if (Directory.Exists(path))
                {
                    return path;
                }
                else
                {
                    return null;
                }
            }

            return null;
        }
    }
}
#endif