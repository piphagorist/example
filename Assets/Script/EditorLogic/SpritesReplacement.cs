#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace SG.EditorLogic
{
    public class SpritesReplacement : EditorWindow
    {
        [MenuItem("Tools/ReplaceSpritesToAtlas")]
        private static void ShowWindow()
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>("Atlases");

            Dictionary<string, string> spriteNameToGUID = new Dictionary<string, string>();
            Dictionary<string, long> spriteNameToID = new Dictionary<string, long>();

            foreach (Sprite sprite in sprites)
            {
                AssetDatabase.TryGetGUIDAndLocalFileIdentifier(sprite, out string guid, out long id);
                
                spriteNameToGUID.Add(sprite.name, guid);
                spriteNameToID.Add(sprite.name, id);
            }

            List<string> spritesGUIDs = new List<string>();
            List<string> assetsPaths = new List<string>();
            foreach (string path in AssetDatabase.GetAllAssetPaths())
            {
                if (path.Contains("Assets/Sprite/"))
                {
                    spritesGUIDs.Add(AssetDatabase.AssetPathToGUID(path));
                    continue;
                }
                if (path[0] == 'P') continue;
                if (path.Contains("TextMesh Pro")) continue;

                Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
                if (obj is GameObject || obj is ScriptableObject)
                {
                    assetsPaths.Add(path);
                }
            }

            foreach(string path in assetsPaths)
            {
                ReplaseSpritesInAsset(path);
            }

            void ReplaseSpritesInAsset(string assetPath)
            {
                string text = File.ReadAllText(assetPath);
                string[] textParts = text.Split('\n');
                for(int i = 0; i < textParts.Length; i++)
                    if (textParts[i].Contains("guid"))
                    {
                        LineGUID line = new LineGUID(textParts[i]);

                        if(spritesGUIDs.Contains(line.guid))
                        {
                            string spritePath = AssetDatabase.GUIDToAssetPath(line.guid);
                            string spriteName = AssetDatabase.LoadAssetAtPath(spritePath, typeof(Object)).name;
                            
                            if (!spriteNameToGUID.ContainsKey(spriteName)) continue;

                            line.guid = spriteNameToGUID[spriteName];
                            line.fileID = spriteNameToID[spriteName];

                            textParts[i] = line.ToString();
                        }
                    }
                
                text = string.Join('\n', textParts);
                File.WriteAllText(assetPath, text);
            }
        }
        
        private class LineGUID
        {
            public string lineName;
            public long fileID;
            public string guid;
            public int type;

            public LineGUID(string data)
            {
                data = data.Replace("}", "");

                string[] dataParts = data.Split('{');
                lineName = dataParts[0];

                string[] fields = dataParts[1].Split(',');

                fileID = long.Parse(fields[0].Split(' ')[1]);
                guid = fields[1].Split(' ')[2];
                type = int.Parse(fields[2].Split(' ')[2]);
            }

            public override string ToString()
            {
                return $"{lineName}{{fileID: {fileID}, guid: {guid}, type: {type}}}";
            }
        }
    }
}
#endif