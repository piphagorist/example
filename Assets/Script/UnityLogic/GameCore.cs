using System.Collections;
using UnityEngine;
using SG.CoreLogic;
using SG.CoreLogic.Configs;
using SG.CoreLogic.Events;
using SG.CoreLogic.Server;
using SG.UnityLogic.UI;
using SG.UnityLogic.PlayerPrefs;
using SG.UnityLogic.Server;
using SG.UnityLogic.Auth;
using SG.UnityLogic.Localization;
using SG.UnityLogic.Sprites;

namespace SG.UnityLogic
{
    public sealed class GameCore : MonoBehaviour
    {
        public static GameCore Instance { get; private set; }
        public ConfigsManager ConfigsManager { get; private set; }
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void CreateInstance()
        {
            GameObject go = new GameObject();
            go.name = typeof(GameCore).Name;
            DontDestroyOnLoad(go);
            Instance = go.AddComponent<GameCore>();

            Instance.Initialization();
        }


        public Camera MainCamera => Camera.main;

        public bool IsEnable { get; private set; }

        public TimeManager TimeManager { get; private set; }
        public CoreManager CoreManager { get; private set; }
        public EventsManager EventsManager { get; private set; }
        public ServerManager ServerManager { get; private set; }
        public LocalizationManager LocalizationManager { get; private set; }
        public PlayerPrefsProvider PlayerPrefsProvider { get; private set; }
        public SpritesManager SpritesManager { get; private set; }


        private bool _isInited;
        private float _currentTime;



        private void Initialization()
        {
            Application.runInBackground = false;

            PlayerPrefsProvider = new PlayerPrefsProvider();

            ServerGameData gameData = new ServerGameData();

            EventsManager = new EventsManager();

            CreateServerManager(gameData);
            ConfigsManager  = new ConfigsManager(Resources.Load<TextAsset>("Config").text);

            SpritesManager = new SpritesManager();

            LocalizationManager = new LocalizationManager();

            new AuthManager().Auth(OnAuthComplete, null);

            void OnAuthComplete()
            {
                TimeManager = new TimeManager(ServerManager, gameData, gameData.serverTime);

                CoreManager = new CoreManager(typeof(GameCore).Assembly, EventsManager,
                ConfigsManager, new System.Random(), TimeManager, ServerManager, gameData);

                ServerManager.Inject();
                StartCoroutine(StartTick());

                Instantiate(Resources.Load<UIWindowsController>("Prefabs/UI/MainCanvas"));

                ServerManager.SetSessionKey(gameData.session.sessionKey);

                _isInited = true;
                IsEnable = true;
            }
        }

        private void CreateServerManager(ServerGameData gameData)
        {
            GameObject go = new GameObject();
            go.name = typeof(ServerManager).Name;
            DontDestroyOnLoad(go);
            ServerManager = go.AddComponent<ServerManager>();
            ServerManager.SetData(gameData);
        }

        private void OnApplicationFocus(bool focus)
        {
            if (!_isInited) return;
            if (focus)
            {
                IsEnable = false;
                TimeManager.SyncTime(() =>
                {
                    IsEnable = true;
                }, true);
            }
            else
                IsEnable = false;
        }

        private IEnumerator StartTick()
        {
            while (true)
            {
                yield return null;

                if (!IsEnable) continue;

                TimeManager.Tick(Time.unscaledTime - _currentTime);
                _currentTime = Time.unscaledTime;
            }
        }
    }
}
