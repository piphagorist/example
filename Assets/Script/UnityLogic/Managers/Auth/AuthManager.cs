﻿using System;
using SG.CoreLogic.Server;

namespace SG.UnityLogic.Auth
{
    public class AuthManager
    {
        const string PLAYER_ID_TEMP = "001";



        public void Auth(Action onComplete, Action onError)
        {
            GameCore.Instance.ServerManager.SetPlayerID(PLAYER_ID_TEMP);

            GameCore.Instance.ServerManager.SendToServer(new AuthRequestJsonData(), onComplete, onError);
        }
    }
}