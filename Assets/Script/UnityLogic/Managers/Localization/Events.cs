using SG.CoreLogic.Events;

namespace SG.UnityLogic.Localization
{
    public readonly struct LanguageChangedEvent : IEvent { }
}