using System;

namespace SG.UnityLogic.Localization
{
    [Serializable]
    public class LocalizationConfig
    {
        public LocalizationItem[] items;
    }


    [Serializable]
    public class LocalizationItem
    {
        public string key;
        public string value;
    }
}