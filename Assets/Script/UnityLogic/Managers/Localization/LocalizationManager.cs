using System.Globalization;
using System.Collections.Generic;
using UnityEngine;
using SG.CoreLogic.Localization;
using SG.CoreLogic.Events;
using SG.UnityLogic.PlayerPrefs;
using Newtonsoft.Json;

namespace SG.UnityLogic.Localization
{
    public sealed class LocalizationManager : ILocalizationManager
    {
        const string RESOURCES_PATH = "Localization/";
        const string PLAYER_PREFS_LANGUAGE = "Language";
        const string DEFAULT_LANGUAGE = "en";


        private PlayerPrefsProvider _playerPrefsProvider;
        private EventsManager _eventsManager;
        private Dictionary<string, string> _items;
        private string _currentLanguage;



        public LocalizationManager()
        {
            _playerPrefsProvider = GameCore.Instance.PlayerPrefsProvider;
            _eventsManager = GameCore.Instance.EventsManager;

            _currentLanguage = _playerPrefsProvider.GetString(PLAYER_PREFS_LANGUAGE, "");

            if (_currentLanguage == "")
                _currentLanguage = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            
            SetLanguage(_currentLanguage);
        }


        public void SetLanguage(string languageCode)
        {
            // TextAsset jsonData = Resources.Load<TextAsset>(RESOURCES_PATH + languageCode);

            // if (jsonData == null)
            // {
            //     if (_items == null)
            //         SetLanguage(DEFAULT_LANGUAGE);

            //     return;
            // }

            // LocalizationConfig config = JsonConvert.DeserializeObject<LocalizationConfig>(jsonData.text);

            _items = new Dictionary<string, string>();

            // foreach (LocalizationItem item in config.items)
            //     _items.Add(item.key, item.value);

            _eventsManager.Push(new LanguageChangedEvent());
        }

        public string GetValueByKey(string key, params object[] args)
        {
            string value = key;

            if (_items.ContainsKey(key))
                value = _items[key];

            return string.Format(value, args);
        }
    }
}
