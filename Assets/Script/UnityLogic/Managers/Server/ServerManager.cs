﻿using UnityEngine;
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.Networking;
using SG.CoreLogic.Server;
using SG.CoreLogic.Events;
using SG.CoreLogic.Currency;
using Newtonsoft.Json;

namespace SG.UnityLogic.Server
{
    public class ServerManager : MonoBehaviour, IServerManager
    {
        const string BASE_URL = "";
        const string JSONSOLT = "";


        private string _sessionKey;
        private string _playerID;
        private long _requestTime;
        private ServerGameData _gameData;

        private EventsManager _eventsManager;

        private JsonSerializerSettings _jsonSettings;


        public long RequestTime => _requestTime;



        public ServerManager()
        {
            _eventsManager = GameCore.Instance.EventsManager;

            _jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public void SetData(ServerGameData gameData)
        {
            _gameData = gameData;
        }

        public void SendToServer(BaseRequestJsonData data, Action callback, Action onError,
            bool withoutSentEvent = false)
        {
            // StartCoroutine(Post(data, callback, onError, withoutSentEvent));
        }

        public void SetPlayerID(string playerID)
        {
            _playerID = playerID;
        }

        public void SetSessionKey(string sessionKey)
        {
            _sessionKey = sessionKey;
        }

        public void Inject()
        {
            
        }


        private string GetHash(string data)
        {
            byte[] byteData = Encoding.ASCII.GetBytes(data);
            byte[] byteHash = MD5.Create().ComputeHash(byteData);
            StringBuilder sb = new StringBuilder();
            foreach (var b in byteHash)
                sb.Append(b.ToString("x2"));
            string hash = sb.ToString();

            return hash;
        }


        private IEnumerator Post(BaseRequestJsonData data, Action callback, Action onError, bool withoutSentEvent)
        {
            string stringData = JsonConvert.SerializeObject(data, _jsonSettings);
            string stringToHash = JSONSOLT + _playerID + stringData;
            string hash = GetHash(stringToHash);

            WWWForm form = new WWWForm();

            if (!string.IsNullOrEmpty(_sessionKey))
                form.AddField("sessionKey", _sessionKey);
            form.AddField("playerId", _playerID);
            form.AddField("data", stringData);
            form.AddField("sign", hash);

            using (UnityWebRequest webRequest = UnityWebRequest.Post(BASE_URL, form))
            {
                webRequest.timeout = 20;
                webRequest.SendWebRequest();

                if (!withoutSentEvent)
                    _eventsManager.Push(new RequestSentEvent());

                float timeBeforeRequest = Time.unscaledTime;

                while (!webRequest.isDone)
                    yield return null;
                    
                if (webRequest.result == UnityWebRequest.Result.Success)
                {
                    string[] hashData = webRequest.downloadHandler.text.Split('!');
                    string testHash = GetHash(JSONSOLT + hashData[1]);

                    if (hashData[0] != testHash)
                    {
                        //TODO данные были изменены
                    }
                    _requestTime = (long)((Time.unscaledTime - timeBeforeRequest) * 1000.0f);
                    JsonConvert.PopulateObject(hashData[1], _gameData);
                    callback();
                }
                else
                {
                    Debug.LogError(webRequest.error);
                    onError?.Invoke();
                }

                _eventsManager.Push(new ResponseGotEvent());
            };
        }
    }
}