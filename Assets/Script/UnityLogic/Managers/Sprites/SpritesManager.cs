﻿using System.Collections.Generic;
using UnityEngine;
using SG.CoreLogic.Configs;

namespace SG.UnityLogic.Sprites
{
    public class SpritesManager
    {
        private Dictionary<string, Sprite> _sprites;



        public SpritesManager()
        {
            _sprites = new Dictionary<string, Sprite>();

            Sprite[] sprites = Resources.LoadAll<Sprite>("Atlases");

            foreach (Sprite sprite in sprites)
            {
                _sprites.Add(sprite.name, sprite);
            }
        }

        
        public Sprite GetSprite(string spriteID)
        {
            Sprite sprite = null;

            if (_sprites.ContainsKey(spriteID))
                sprite = _sprites[spriteID];

            return sprite;
        }

        public Sprite GetSprite(ConfigWithIdSprite config)
        {
            if (config == null) return null;
            
            return GetSprite(config.SpriteID);
        }
    }
}
