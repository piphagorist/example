﻿namespace SG.UnityLogic.PlayerPrefs
{
    public sealed class PlayerPrefsProvider
    {
        public string GetString( string key, string defaultValue = "" )
        {
            return UnityEngine.PlayerPrefs.GetString(key, defaultValue);
        }

        public void SetString( string key, string value )
        {
            UnityEngine.PlayerPrefs.SetString(key, value);
        }
    }
}