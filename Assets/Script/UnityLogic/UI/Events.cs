﻿using SG.CoreLogic.Events;

namespace SG.UnityLogic.UI
{
    public readonly struct ShowWindowEvent : IEvent
    {
        public readonly IWindowData Data;



        public ShowWindowEvent(IWindowData data)
        {
            Data = data;
        }
    }



    public readonly struct HideWindowEvent : IEvent
    {
        public readonly IWindowData Data;



        public HideWindowEvent(IWindowData data)
        {
            Data = data;
        }
    }



    public readonly struct HideActiveWindowEvent : IEvent { }
}