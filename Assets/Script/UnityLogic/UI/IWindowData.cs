namespace SG.UnityLogic.UI
{
    public interface IWindowData
    {
        internal abstract string PrefabName { get; }
    }
}
