﻿using UnityEngine;
using SG.CoreLogic.Events;
using SG.UnityLogic.Localization;
using TMPro;

namespace SG.UnityLogic.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedText : MonoBehaviour
    {
        [SerializeField] private string key = default;

        private EventsManager _eventsManager;
        private LocalizationManager _localizationManager;
        private TextMeshProUGUI _text;

        private void Awake()
        {
            _eventsManager = GameCore.Instance.EventsManager;
            _localizationManager = GameCore.Instance.LocalizationManager;

            _text = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable()
        {
            _eventsManager.SubscribeTo<LanguageChangedEvent>(LanguageChangedHandler);

            UpdateText();
        }

        private void OnDisable()
        {
            _eventsManager.UnSubscribeFrom<LanguageChangedEvent>(LanguageChangedHandler);
        }

        private void UpdateText()
        {
            _text.text = _localizationManager.GetValueByKey(key);
        }

        private void LanguageChangedHandler(LanguageChangedEvent obj)
        {
            UpdateText();
        }
    }
}
