using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using SG.CoreLogic.Events;
using SG.CoreLogic.Server;

namespace SG.UnityLogic.UI
{
    public sealed class UIWindowsController : MonoBehaviour
    {
        private const string RESOURCES_WINDOWS_PATH = "Prefabs/UI/Windows/";


        [SerializeField] private Canvas blocker = default;
        [SerializeField] private Canvas popUpBlocker = default;


        private Dictionary<Type, Window> _windows;

        private Window _activeWindow;
        private Window _activePopUp;

        private EventSystem _eventSystem;

        private EventsManager _eventsManager;
        private int _popUpSortingOrder = 100;



        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            _windows = new Dictionary<Type, Window>();

            _eventsManager = GameCore.Instance.EventsManager;

            _eventSystem = EventSystem.current;

            popUpBlocker.sortingOrder = _popUpSortingOrder - 1;
            popUpBlocker.gameObject.SetActive(false);

            _eventsManager.SubscribeTo<ShowWindowEvent>(ShowWindowHandler);
            _eventsManager.SubscribeTo<HideWindowEvent>(HideWindowHandler);
            _eventsManager.SubscribeTo<HideActiveWindowEvent>(HideActiveWindowHandler);
            _eventsManager.SubscribeTo<RequestSentEvent>(BlockUIHandler);
            _eventsManager.SubscribeTo<ResponseGotEvent>(UnblockUIHandler);
        }

        private void ShowWindowHandler(ShowWindowEvent e)
        {
            ShowWindow(e.Data);
        }

        private void HideWindowHandler(HideWindowEvent e)
        {
            Type type = e.Data.GetType();

            if (!_windows.ContainsKey(type))
                return;

            Window currentWindow = _windows[type];

            currentWindow.Hide();

            if (currentWindow.IsStaticWindow) return;

            if (currentWindow.IsPopUp)
            {
                popUpBlocker.gameObject.SetActive(false);
                return;
            }

            _activeWindow = null;
        }

        private void HideActiveWindowHandler(HideActiveWindowEvent e)
        {
            if (_activeWindow == null) return;

            _activeWindow.Hide();

            _activeWindow = null;
        }

        private void BlockUIHandler(RequestSentEvent _)
        {
            _eventSystem.enabled = false;
        }

        private void UnblockUIHandler(ResponseGotEvent _)
        {
            _eventSystem.enabled = true;
        }

        private void ShowWindow(IWindowData data)
        {
            Type type = data.GetType();

            if (!_windows.ContainsKey(type))
                LoadWindowFromResources(data.PrefabName);

            Window windowForShowing = _windows[type];

            if (windowForShowing.IsStaticWindow)
            {
                windowForShowing.Show(data);
                return;
            }

            if (windowForShowing.IsPopUp)
            {
                _activePopUp = windowForShowing;
                _activePopUp.SetSortingOrder(_popUpSortingOrder);
                _activePopUp.Show(data);

                popUpBlocker.gameObject.SetActive(true);

                return;
            }

            if (_activeWindow != null)
                _activeWindow.Hide();

            _activeWindow = windowForShowing;

            windowForShowing.Show(data);

            blocker.sortingOrder = transform.childCount - 1;
            windowForShowing.SetSortingOrder(transform.childCount);
        }

        private Window LoadWindowFromResources(string prefabName)
        {
            GameObject window = Resources.Load<GameObject>(RESOURCES_WINDOWS_PATH + prefabName);
            Window instance = Instantiate(window, transform).GetComponent<Window>();
            instance.gameObject.SetActive(false);
            instance.InternalInject();
            instance.InternalInit();
            _windows.Add(instance.Type, instance);

            return instance;
        }

        private void PreloadWindow(string prefabName)
        {
            Window instance = LoadWindowFromResources(prefabName);

            ShowWindow(Activator.CreateInstance(instance.Type) as IWindowData);
        }
    }
}
