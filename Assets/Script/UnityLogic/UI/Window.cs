using SG.CoreLogic.Events;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace SG.UnityLogic.UI
{
    [RequireComponent(typeof(Canvas), typeof(CanvasScaler), typeof(GraphicRaycaster))]
    public abstract class Window : MonoBehaviour
    {
        [SerializeField] private bool isStaticWindow = default;
        [SerializeField] private bool isPopUp = default;
        [SerializeField] private Button closeBtn = default;

        internal bool IsStaticWindow => isStaticWindow;
        internal bool IsPopUp => isPopUp;
        internal bool IsActive => gameObject.activeSelf;
        internal abstract Type Type { get; }


        protected RectTransform CanvasRect { get; private set; }
        protected EventsManager EventsManager { get; private set; }


        private bool _firstShowing = true;
        private Canvas _canvas;


        protected virtual void Inject() { }
        protected virtual void Init() { }
        protected virtual void PrepareForShowing() { }
        protected virtual void PrepareForHiding() { }
        protected virtual void Subscribe() { }
        protected virtual void UnSubscribe() { }
        protected virtual void HideItself() { }


        protected void Push<T>(T data) where T : IEvent => EventsManager.Push(data);
        protected void SubscribeTo<T>(Action<T> action) where T : IEvent => EventsManager.SubscribeTo(action);
        protected void UnSubscribeFrom<T>(Action<T> action) where T : IEvent => EventsManager.UnSubscribeFrom(action);



        internal void InternalInject()
        {
            CanvasRect = (RectTransform)transform;

            _canvas = GetComponent<Canvas>();

            EventsManager = GameCore.Instance.EventsManager;
        }

        internal void InternalInit()
        {
            if (closeBtn == null) return;

            closeBtn.onClick.AddListener(HideItself);
        }

        internal virtual void Show()
        {
            gameObject.SetActive(true);

            if (_firstShowing)
            {
                Inject();
                Init();

                _firstShowing = false;
            }

            PrepareForShowing();
            Subscribe();
        }

        internal virtual void Show(IWindowData data)
        {
            Show();
        }

        internal void Hide()
        {
            gameObject.SetActive(false);

            PrepareForHiding();
            UnSubscribe();
        }

        internal void SetSortingOrder(int sortingOrder)
        {
            _canvas.sortingOrder = sortingOrder;
        }
    }



    public class Window<T> : Window where T : IWindowData
    {
        internal override Type Type => Data.GetType();


        protected T Data { get; private set; }



        internal override void Show()
        {
            base.Show();
        }

        internal override void Show(IWindowData data)
        {
            Data = (T)data;

            base.Show(data);
        }


        protected override void HideItself()
        {
            Push(new HideWindowEvent(Data));
        }
    }
}
